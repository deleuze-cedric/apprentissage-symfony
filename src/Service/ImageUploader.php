<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class ImageUploader
{

    public function __construct(
        protected SluggerInterface $slugger
    )
    {
    }

    public function upload(UploadedFile $image, string $targetRepository): string
    {
        $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);

        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $image->guessExtension();

        // Move the file to the directory where brochures are stored
            $image->move(
                $targetRepository,
                $newFilename
            );

            return $newFilename;
    }

}