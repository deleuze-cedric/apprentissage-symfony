<?php

namespace App\Controller;

use App\Entity\Pet;
use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Service\ImageUploader;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/post')]
class PostController extends AbstractController
{

    #[Route('/', name: 'app_post_index', methods: ['GET'])]
    public function index(PostRepository $postRepository): Response
    {
        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findAll(),
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/new', name: 'app_post_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PostRepository $postRepository, ImageUploader $imageUploader): Response
    {
        $post = new Post();
        $pet = $this->getConnectedPet();
        $post->setAuthor($pet);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->uploadImage($form, $post, $imageUploader);

            $postRepository->add($post);

            return $this->redirectToRoute('app_post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('post/new.html.twig', [
            'post' => $post,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_post_show', requirements: ['id' => '\d'], methods: ['GET'])]
    public function show(Post $post): Response
    {
        return $this->render('post/show.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/mes-posts', name: 'app_post_list_posts_of_connected_pet', methods: ['GET'])]
    public function listPostOfConnectedPet(PostRepository $postRepository): Response
    {
        $pet = $this->getConnectedPet();

        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->listPostsOfPet($pet),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_post_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Post $post, PostRepository $postRepository,  ImageUploader $imageUploader): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->uploadImage($form, $post, $imageUploader);

            $postRepository->add($post);

            return $this->redirectToRoute('app_post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('post/edit.html.twig', [
            'post' => $post,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_post_delete', methods: ['POST'])]
    public function delete(Request $request, Post $post, PostRepository $postRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $post->getId(), $request->request->get('_token'))) {
            $postRepository->remove($post);
        }

        return $this->redirectToRoute('app_post_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @throws Exception
     */
    protected function getConnectedPet(): Pet
    {
        $pet = $this->getUser();

        if (!empty($pet) && $pet instanceof Pet) {
            return $pet;
        }

        throw new Exception('Connected user is not a pet');
    }

    protected function uploadImage(FormInterface $form, Post $post, ImageUploader $imageUploader){

        /** @var UploadedFile $image */
        $image = $form->get('image')->getData();

        if ($image) {
            // try {
            $newFilename = $imageUploader->upload($image, $this->getParameter('images_directory'));

            // updates the 'image' property to store the PDF file name
            // instead of its contents
            $post->setImage($newFilename);
            // } catch (FileException $e) {
            //     // ... handle exception if something happens during file upload
            //     $this->addFlash('error', $e->getMessage());
            // }

        }
    }

}
